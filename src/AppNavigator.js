import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';

//import ContactList from './containers/ContactsList';
import HomeScreen from './components/HomeScreen';
import LandingScreen from './components/LandingScreen';




const MyApp = TabNavigator({
	logo: {
	screen: LandingScreen,
    navigationOptions:{
     header:null,
     tabBarLabel: <Text></Text>,
      tabBarIcon:({tintColor}) => (
          <Image
        source={require('../assets/Logo.png')}
        style={{
        	width: 75,
    resizeMode: 'contain'}}
      />
        )
    }
  },
   home: {
    screen: LandingScreen,
    navigationOptions:{
     header:null,
     tabBarLabel: <Text></Text>,
      tabBarIcon:({tintColor}) => (
          <Image
        source={require('../assets/Home_Btn_nrm.png')}
        style={[styles.icon, {tintColor: tintColor}]}
      />
        )
    }
  },
   orders: {
    screen: HomeScreen,
    navigationOptions:{
      // header:null,
      headerTitleStyle: { alignSelf:'center' },
      headerStyle : { backgroundColor: '#FFA500'},
			title: 'Orders',
			tabBarVisible:true,
			headerLeft: <Text style={{fontSize:10}}>We are happy to serve you!</Text>,
			headerRight: <View style={{flexDirection: 'row'}}><Image style={{marginRight: 15}} source={require("../assets/Cart.png")} /><Image style={{marginRight: 15}} source={require("../assets/Search_21x21.png")} /></View>,
      tabBarLabel: <Text></Text>,
      tabBarIcon:({tintColor}) => (
         <Image
        source={require('../assets/Order_Btn_nrm.png')}
        style={[styles.icon, {tintColor: tintColor}]}
      />
        )
    }
  },
	menu: {
    screen: HomeScreen,
    navigationOptions:{
    	headerTitleStyle: { alignSelf:'center' },
      headerStyle : { backgroundColor: '#FFA500'},
			title: 'Menu',
			tabBarVisible:true,
			headerLeft: <Text style={{fontSize:10}}>We are happy to serve you!</Text>,
			headerRight: <View style={{flexDirection: 'row'}}><Image style={{marginRight: 15}} source={require("../assets/Cart.png")} /><Image style={{marginRight: 15}} source={require("../assets/Search_21x21.png")} /></View>,
      tabBarLabel: <Text></Text>,
      tabBarIcon:({tintColor}) => (
         <Image
        source={require('../assets/Menu_Btn_nrm.png')}
        style={[styles.icon, {tintColor: tintColor}]}
      />
        )
    }
  },
 
  notifications: {
    screen: HomeScreen,
    navigationOptions:{
      // header:null,
      tabBarLabel: <Text></Text>,	
      tabBarIcon:({tintColor}) => (
         <Image
        source={require('../assets/Notifi_Btn_nrm.png')}
        style={[styles.icon, {tintColor: tintColor}]}
      />
        )
    }
  },
}, {
  tabBarOptions: {
  activeTintColor: 'black',
  activeBackgroundColor:'orange',
  inactiveTintColor: 'grey',
  showIcon: true,
  upperCaseLabel:false,
  showLabel: false,
  labelStyle: {
    fontSize: 0,
  },
  indicatorStyle:{
    backgroundColor: 'orange',
    height:0
  },
  style: {
    backgroundColor: 'transparent',
    height:53
    // borderBottomColor:'black'

  },
},

  tabBarPosition:'bottom',
  swipeEnabled:true,
});

const AppNavigator = StackNavigator({
	Home:{
		screen: MyApp,
		navigationOptions:{
			}
	},
/*	Contacts: {
		screen: ContactList,
		navigationOptions: ({navigation}) => ({
	      title: 'Your Contacts',
	    }),
	} */
},
{
	initialRouteName: 'Home',
	//navigationOptions: provide default nav opts for screen here
});

const styles = StyleSheet.create({
  
   icon: {
    width: 52,
    height: 43,
    padding: 0,
    marginBottom: 0
  },
});

export default AppNavigator;
