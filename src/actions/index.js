export const GET_ITEMS = "getItems";
import {NavigationActions} from 'react-navigation';

const items_data = {
	dummyItems: [{"name": "Cafe 5H By the Kitchen", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu1.png") },
    {"name": "Truffles Ice and Spice", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu2.png") },
    {"name": "Beijing Bites", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu3.png") },
    {"name": "Londoners", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu4.png") },
    {"name": "Big Wong", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu5.png") },
    {"name": "Tamura", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu6.png") },
    {"name": "NYC Pie", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu7.png") },
    {"name": "Moets Curry Leaf", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu8.png") },
    {"name": "Dzoku Tribal Kitchen", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu9.png") },
    {"name": "Cafe 5H By the Kitchen", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu1.png") },
    {"name": "Truffles Ice and Spice", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu2.png") },
    {"name": "Beijing Bites", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu3.png") },
    {"name": "Londoners", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu4.png") },
    {"name": "Big Wong", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu5.png") },
    {"name": "Tamura", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu6.png") },
    {"name": "NYC Pie", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu7.png") },
    {"name": "Moets Curry Leaf", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu8.png") },
    {"name": "Dzoku Tribal Kitchen", "type" : "Lowrence Road, Casual Dining", "status" : "Open now", "rating": "3.9", "img": require("../../assets/imgmenu9.png") }
]}

export function getItemsList(){
	return {
		type: GET_ITEMS,
		payload: items_data.dummyItems
	}
}

export function resetNavigation(dispatch, routeName) {
	dispatch(NavigationActions.reset({
		index: 0,
		actions: [
			NavigationActions.navigate({ routeName: routeName})
		]
	}))
}