'use strict';
import React, {Component} from 'react';

import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { getItemsList } from '../actions';

var width = Dimensions.get('window').width;
class HomeScreen extends Component {

  constructor(props) {
    super(props);
    var _pressData;
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      itemsList: this.props.items,
      dataSource: ds.cloneWithRows(this.props.items.items),
    };
  }

  componentWillMount() {
    this.props.getItemsList();
    this._pressData = {};
  }

  componentDidMount() {
    console.log(this.props.items.items)
  }


  render() {
    return (
      <View>
      <Image style={styles.headImage} source={require("../../assets/Head_Image.png")} />
      <ListView contentContainerStyle={styles.list}
      enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderRow={this._renderRow}
      />
      </View>
    );
  }

  _renderRow(rowData: object, sectionID: number, rowID: number) {
    return (
      <TouchableHighlight  underlayColor='rgba(0,0,0,0)'>
        <View>
          <View style={styles.row}>
            <Image style={styles.thumb} source={rowData.img} />
            <Text style={styles.nameText}>
            {rowData.name}
            </Text>
            <Text style={styles.typeText}>
              {rowData.type}
            </Text>
            <View style={styles.column}>
            <Text style={styles.statusText}>
              {rowData.status}
            </Text>
            <Text style={styles.ratingText}>
              {rowData.rating}
            </Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

}

function mapStateToProps(state){
  return { 
    items: state,
  };
}


export default connect(mapStateToProps, { getItemsList })(HomeScreen);

var styles = StyleSheet.create({
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  row: {
    justifyContent: 'center',
    padding: 0,
    margin: 0,
    width: 120,
    height: 120,
    backgroundColor: '#F6F6F6',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 0,
    borderColor: '#CCC'
  },
  column: {
    flexDirection: 'row',
    flex: 0.1
  },
  thumb: {
    width: 120,
    height: 64
  },
  nameText: {
    flex: 0.1,
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 8
  },
  typeText: {
    flex: 0.2,
    marginTop:0,
    fontSize: 8
  },
   statusText: {
    marginTop:0,
    fontSize: 8,
    color: 'orange',
   flex: 0.3,
   marginLeft: 12
  },
  ratingText: {
    justifyContent: 'flex-end',
    fontSize: 8,
    color: 'orange',
    marginRight: 15
  },
  headImage: {
    flex: 0.4,
    width: width
  }
});