'use strict';
import React, {Component} from 'react';

import {
  AppRegistry,
  ImageBackground,
  ListView,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class LandingScreen extends Component {

  constructor(props) {
    super(props);
  }

render() {
  return(
    <View style={styles.imageContainer}>
    <ImageBackground style={styles.image} source={require("../../assets/landing-page.png")}>
    <View style={{marginTop: 200, marginLeft: 30, backgroundColor: 'rgba(0,0,0,0)'}}>
    <Text style={{ color: 'white', fontSize: 28}}>Food</Text>
    <Text style={{ color: 'white', fontSize: 44}}>Panda</Text>
    <Text style={{ color: 'white', fontSize: 12, fontWeight: 'bold', marginTop: 10}}>WHAT A TWIST!</Text>
    <Text style={{ color: 'white', fontSize: 8, width: 200, marginTop: 10}}>The Panda, the iconic long, slim slick of bread, has traditionally one of the most potent symbols of French culture.</Text>
    </View>
    </ImageBackground>
    </View>
  )
}
}

var styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    alignItems: 'stretch'
  },
  image: {
    flex: 1,
    height: height,
    width: width
  }
});
