import { GET_ITEMS } from '../actions';

export function itemsReducer(state=[],action){
	switch(action.type){
		case GET_ITEMS:{
			return action.payload;
		}
		default: {
			return state;
		}
	}
}