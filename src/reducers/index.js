import { combineReducers } from 'redux';
import {itemsReducer} from './ItemsReducer';

const AppReducer = combineReducers({
	items: itemsReducer,
});

export default AppReducer;